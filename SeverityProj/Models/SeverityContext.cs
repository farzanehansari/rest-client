﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace SeverityProj.Models
{
    public class SeverityContext : DbContext
    {
        public SeverityContext(DbContextOptions<SeverityContext> options)  : base(options)
        {

        }
        public DbSet<Severity> Severities { get; set; }
        public DbSet<SubSeverity> SubSeverities { get; set; }
    }
}
