﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeverityProj.Models
{
    public interface ISeverity
    {
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        [JsonProperty("systemSerialNumber")]
        public int systemSerialNumber { get; set; }
        [JsonProperty("tubeSerialNumber")]
        public int tubeSerialNumber { get; set; }
    }    

    public class Severity : ISeverity
    {
        public int Id { get; set; }
        public int systemSerialNumber { get; set; }
        public int tubeSerialNumber { get; set; }
        public string plane { get; set; }
        public DateTime dateTimeThMax { get; set; }
        public int generalSeverity { get; set; }
    }  
    public class SubSeverity : ISeverity
    {
        public int Id { get; set; }
        public int systemSerialNumber { get; set; }
        public int tubeSerialNumber { get; set; }
        public int systemMaterialNumber { get; set; }
        public string systemName { get; set; }
        public int tubeMaterialNumber { get; set; }
        public string tubeName { get; set; }

     }    
    
}
