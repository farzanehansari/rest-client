﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeverityProj.Models;

namespace SeverityProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeverityController : ControllerBase
    {
        private readonly SeverityContext _context;

        public SeverityController(SeverityContext context)
        {
            _context = context;
        }

        // GET: api/Severity        
        [HttpGet]
        public ActionResult<IEnumerable<Severity>> GetSeverities()
        {
           return  _context.Severities.ToList();
        }


        // GET: api/Severity/severityasync
        [HttpGet]
        [Route("severityasync")]
        public async Task<ActionResult<IEnumerable<Severity>>> GetAsyncSeverities()
        {
            return await _context.Severities.ToListAsync();
        }

        // GET: api​/severity​/severityes/10
        [HttpGet("severityes/{sysSerialNr}")]

        public async Task<ActionResult<IEnumerable<SubSeverity>>> GetSeverities(int sysSerialNr)
        {
            var severityList = await _context.SubSeverities.Where(x => x.systemSerialNumber == sysSerialNr).ToListAsync();

            if (severityList == null)
            {
                return NotFound();
            }

            return severityList;
        }

        // GET: api/Severity/5      
        [HttpGet("{id}")]        
        public async Task<ActionResult<Severity>> GetSeverity(int id)
        {
            var severity = await _context.Severities.FindAsync(id);

            if (severity == null)
            {
                return NotFound();
            }

            return severity;
        }     


        // POST: api/Severity
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Severity>> PostSeverity(Severity severity)
        {
            _context.Severities.Add(severity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSeverity", new { id = severity.Id }, severity);
            

        }

        // POST: api/Severity/Severityes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Route("severityes")]
        public async Task<ActionResult<SubSeverity>> PostSeverityes(SubSeverity subseverity)
        {
            _context.SubSeverities.Add(subseverity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSeverities", new { id = subseverity.Id }, subseverity);


        }


        // PUT: api/Severity/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSeverity(int id, Severity severity)
        {
            if (id != severity.Id)
            {
                return BadRequest();
            }

            _context.Entry(severity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SeverityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Severity/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Severity>> DeleteSeverity(int id)
        {
            var severity = await _context.Severities.FindAsync(id);
            if (severity == null)
            {
                return NotFound();
            }

            _context.Severities.Remove(severity);
            await _context.SaveChangesAsync();

            return severity;
        }

        private bool SeverityExists(int id)
        {
            return _context.Severities.Any(e => e.Id == id);
        }
    }
}
