## Project Instruction

Create a **Web API Project with ASP.Net Core 3.1 ** VS 2019 16.7.2.

Add **Model** Class **Severity.cs**

Interface **ISeverity { int Id [Key] , int systemSerialNumber , tubeSerialNumber} **

Class **Severity: ISeverity**

Class **SubSeverity : ISeverity**

Retrieve JSON String Format Fields from Model.

Create database context as **DbSet<Severity>** Severities , **DbSet<SubSeverity>** SubSeverities, With **Microsoft.EntityFrameworkCore.DbContext**   

Add **NuGet package** in Project **Microsoft.EntityFrameworkCore.InMemory ** , **Microsoft.EntityFrameworkCore.SQLServer**
 
Add DBContext in **Configuration Service in Stratup.cs**

Add API Controller with actions, using Entity Framework as **Scaffolded Item SeverityController.cs** 
 
**[Route("api/[controller]")]** = > api/severity

**[ApiController] **

**Get : [HttpGet]** Retrieve Data

**Post : [HttpPost]** Sent Request and Create Entity

**Put:  [HttpPut("{id}")]** Update Data

**Delete**: Delete Data

---

## Run Project


1. Run Project https://localhost:44345/api/severity

2. Install [Postman]( https://www.postman.com/downloads/) to Create Post request.
   Disable **SSL certificate verification**  From **File -> Settings (General tab)**  
   **PostSeverity Method** in SeverityController create Severity data in Memory,** { "Id" : 1,    "systemSerialNumber" : 10 ,  "tubeSerialNumber" : 150,  "plane" : "Aisbus",  "dateTimeThMax" : "2020.08.21",  "generalSeverity" : 100 }**

3. https://localhost:44345/**api/severity** : Return List of Severity as JSON String result

4. https://localhost:44345/**api/severity/severityasync** , Return List of Severity Async as JSON String result

5. https://localhost:44345/**api/severity/severityes/10** : Send Post request in Postman to create request.
   **PostSeverityes Method in SeverityController** create Severity data in Memory.
   **{ "Id" : 1,    "systemSerialNumber" : 10 ,  "tubeSerialNumber" : 160,  "systemMaterialNumber" : 120,  "systemName" : "Infoteam",  "tubeMaterialNumber" : 150,  "tubeName": "Tube1"}**


---
